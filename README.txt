INTRODUCTION
------------
The Nofollow Noindex module is a very simple approach to add meta robots "nofollow noindex" to the HTML head of selected routes. 
Simple configuration to prevents the site from search engines based on the domain name or relative path.
<meta name="robots" content="noindex, nofollow" />


REQUIREMENTS
------------
This module requires no modules outside of Drupal core.


INSTALLATION
------------
Install the Nofollow Noindex as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further information.

1) Copy the nofollow noindex folder in the modules folder in your Drupal directory. 
   or use composer

2) Enable the module using Manage -> Extend (/admin/modules).


CONFIGURATION
-------------
Enable the settings at
Manage -> Configuration -> Search and Meta data -> Nofollow Noindex Settings
(/admin/config/search/nofollow-noindex).
  Enable - Enable the functionality
  Add nofollow,noindex Routes - A list of routes that you want to be add no-follow no-index. Wildcard "*" is supported.
    Domain path: A list of host routes (with out http:// or https://). If the domain name http://qa.example.com means just add qa.*
    Example:
      local.*
      qa.*
  Relative path: A list of Relative url with slash(/)
    Example:
      /admin/*
      /news/*
      /register/webinar
